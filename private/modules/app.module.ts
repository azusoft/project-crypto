import { IndexController } from '../controllers';
import { Module } from '@nestjs/common';
import { UsersGateway, MessagesGateway } from '../gateways';

@Module({
	components: [ UsersGateway, MessagesGateway ],
	controllers: [ IndexController ]
})
export class ApplicationModule {}
