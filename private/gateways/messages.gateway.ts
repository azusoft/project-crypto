import { IMessage } from '../models';
import { WebSocketGateway, SubscribeMessage, WsResponse, WebSocketServer } from '@nestjs/websockets';
import * as SocketIO from 'socket.io-client';

@WebSocketGateway({ port: 8081, namespace: 'messages' })
export class MessagesGateway {
	@WebSocketServer() 
	private _server: any;

	@SubscribeMessage('joinToRoom')
	onJoinToRoom(client: SocketIO, room: string): WsResponse<boolean> {
		client.join(room);
		let event = 'acceptJoin';
		return ({ event, data: true });
	}

	@SubscribeMessage('sendMessage')
	onSendMessage(client: SocketIO, message: IMessage) {
		this._server.to('publicChat').emit('newMessage', message);
	}
}