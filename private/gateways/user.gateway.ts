import 'rxjs/add/observable/from';
import 'rxjs/add/operator/map';
import { IUser } from '../models';
import { Observable } from 'rxjs/Observable';
import { SqLite } from '../libs/sqLite';
import { WebSocketGateway, SubscribeMessage, WsResponse } from '@nestjs/websockets';
import * as SocketIO from 'socket.io-client';

@WebSocketGateway({ port: 8081, namespace: 'users' })
export class UsersGateway {
	private _sqLite: SqLite = SqLite.getInstance();

	@SubscribeMessage('registartion')
	onRegistartion(client: SocketIO, user: IUser): WsResponse<boolean> {
		let query = this._sqLite.newUser(user);
		if (query === false) {
			let event = 'deniedRegistartion';
			return ({ event, data: false });
		} else {
			let event = 'acceptRegistartion';
			console.log(query);
			return ({ event, data: true });
		}
	}

	@SubscribeMessage('login')
	onLogin(client: SocketIO, user: any): WsResponse<IUser | boolean> {
		let query = this._sqLite.loginUser(user);
		if (query === undefined) {
			let event = 'deniedLogin';
			return ({ event, data: false });
		} else {
			let event = 'acceptLogin';
			let loggedInClient: IUser = user.user;
			loggedInClient.id = query.id;
			return ({ event, data: loggedInClient });
		}
	}
}