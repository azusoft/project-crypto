import { Controller, Get, Param, Req, Res, HttpStatus } from '@nestjs/common';
import { Request, Response } from 'express';
import * as Env from 'env-var';

@Controller('*')
export class IndexController {
	@Get()
	getIndex(@Req() req: Request, @Res() res: Response) {
		res.render('app', { title: 'Project Crypto' });
	}
}