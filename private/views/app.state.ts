import { IUser } from '../models';

export default interface State {
	userIsLoggedIn: boolean;
	user: IUser;
}