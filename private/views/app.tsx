import { IUser, IMessage } from '../models';
import * as React from 'react';
import * as ReactObserver from 'react-event-observer';
import * as SocketIO from 'socket.io-client';
import App from './components/app';
import BrowserUtility from '../libs/browserUtility';
import State from './app.state';

export default class extends React.Component<{}, State> {
	private _observer;
	private _socket: SocketIO;

	constructor(props) {
		super(props);
		if (BrowserUtility.isBrowser) {
			this._loginUser = this._loginUser.bind(this);
			this._logoutUser = this._logoutUser.bind(this);
			this._setUserLoginState = this._setUserLoginState.bind(this);
			this._connectToMessagingService = this._connectToMessagingService.bind(this);
			this._sendMessage = this._sendMessage.bind(this);
			this._registrateUser = this._registrateUser.bind(this);
			this._observer = ReactObserver();
		}
		this.state = {
			userIsLoggedIn: false,
			user: {}
		}
	}

	componentWillUnmount() {
		this._observer.unsubscribe('loginUser');
		this._observer.unsubscribe('logoutUser');
		this._observer.unsubscribe('sendMessage');
	}

	componentDidMount() {
		this._observer.subscribe('loginUser', this._loginUser);
		this._observer.subscribe('logoutUser', this._logoutUser);
		this._observer.subscribe('registrateUser', this._registrateUser);
	}

	_loginUser(user: IUser) {
		this._socket = SocketIO('http://localhost:8081/users');
		this._socket.on('connect', () => {
			this._socket.emit('login', { user });
		});
		this._socket.on('acceptLogin', (loggedInClient: IUser) => {
			this._setUserLoginState(true, loggedInClient);
			this._observer.publish('acceptUserLogin');
			this._socket.close();
			this._connectToMessagingService();
		});
		this._socket.on('deniedLogin', (loggedInClient: boolean) => {
			this._socket.close();
			this._observer.publish('swipeToScreen', { id: 'LoginDialog', screen: 0 });
		});
	}

	_logoutUser(user: IUser) {
		this._socket.close();
		this._setUserLoginState(false);
	}

	_registrateUser(user: IUser) {
		this._socket = SocketIO('http://localhost:8081/users');
		this._socket.on('connect', () => {
			this._socket.emit('registartion', { user });
		});
		this._socket.on('acceptRegistartion', (isSuccess: boolean) => {
			this._socket.close();
			this._observer.publish('loginUser', user);
		});
		this._socket.on('deniedRegistartion', (isSuccess: boolean) => {
			this._socket.close();
			this._observer.publish('swipeToScreen', { id: 'LoginDialog', screen: 0 });
		});
	}

	_setUserLoginState(loginState: boolean, loggedInClient: IUser = {}) {
		this.setState({
			userIsLoggedIn: loginState,
			user: loggedInClient,
		});
	}

	_connectToMessagingService() {
		this._socket = SocketIO('http://localhost:8081/messages');
		this._socket.on('connect', () => {
			this._socket.emit('joinToRoom', 'publicChat');
		});
		this._socket.on('newMessage', (message: IMessage) => {
			this._observer.publish('newMessage', message);
		});
		this._socket.on('acceptJoin', (message: boolean) => {
			this._observer.subscribe('sendMessage', this._sendMessage);
		});
		this._socket.on('disconnect', () => {
			this._observer.unsubscribe('sendMessage');
		});
	}

	_sendMessage(message: string) {
		this._socket.emit('sendMessage', { message, sender: this.state.user.email } as IMessage);
	}

	render() {
		return <App observer={ this._observer } />;
	}
}