import { IUser } from '../../../models';
import * as React from 'react';
import BrowserUtility from '../../../libs/browserUtility';
import Button from 'material-ui/Button';
import { DialogActions, DialogContent, DialogContentText, DialogTitle } from 'material-ui/Dialog';
import Props from './loginDialog.props';
import State from './loginDialog.state';
import Style from './loginDialog.style';
import TextField from 'material-ui/TextField';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import * as Validator from 'validator';

export default class extends React.Component<Props, State> {
	private _observer;

	constructor(props) {
		super(props);
		if (BrowserUtility.isBrowser) {
			this._closeLoginDialog = this._closeLoginDialog.bind(this);
			this._loginUser = this._loginUser.bind(this);
			this._handleChange = this._handleChange.bind(this);
			this._registrateUser = this._registrateUser.bind(this);
			this._observer = this.props.observer;
		}
		this.state = {
			email: '',
			password: '',
			badEmail: false,
			badPassword: false
		}
	}

	_closeLoginDialog() {
		this._observer.publish('closeDialog', this.props.id);
	}

	_loginUser() {
		if (this._inputIsValid()) {
			let user: IUser = { email: this.state.email, password: this.state.password } as IUser;
			this._observer.publish('loginUser', user);
			this._observer.publish('swipeToScreen', { id: this.props.id, screen: 1 });
		}
	}

	_registrateUser() {
		if (this._inputIsValid()) {
			let user: IUser = { email: this.state.email, password: this.state.password } as IUser;
			this._observer.publish('registrateUser', user);
			this._observer.publish('swipeToScreen', { id: this.props.id, screen: 1 });
		}
	}

	_inputIsValid(): boolean {
		if (!this.state.badEmail && !this.state.badPassword &&
			this.state.email !== '' && this.state.password !== '') {
			return true;
		} else {
			return false;
		}
	}

	_handleChange = name => event => {
		switch (name) {
			case 'email':
				if (Validator.isEmail(event.target.value)) {
					this.setState({ badEmail: false });
				} else {
					this.setState({ badEmail: true });
				}
				break;
			case 'password':
				if (event.target.value.toString().trim().length >= 6) {
					this.setState({ badPassword: false });
				} else {
					this.setState({ badPassword: true });
				}
				break;
			default:
				console.error('Wrong input');
				break;
		}
		this.setState({ [name]: event.target.value });
	}

	render() {
		return (
			<main>
				<DialogTitle>
					Login to your account
				</DialogTitle>
				<DialogContent>
					<DialogContentText>
						To login to this website, please enter your
						email address and your password here.
					</DialogContentText>
					<TextField
						autoFocus
						required
						error={ this.state.badEmail }
						margin='dense'
						id='email'
						label='Email Address'
						type='email'
						fullWidth
						value={ this.state.email }
						onChange={ this._handleChange('email') }
					/>
					<TextField
						required
						error={ this.state.badPassword }
						margin='dense'
						id='password'
						label='Password'
						type='password'
						fullWidth
						onChange={ this._handleChange('password') }
					/>
				</DialogContent>
				<DialogActions>
					<Button onClick={ this._closeLoginDialog } color='primary'>
						Cancel
					</Button>
					<Button disabled={ !this._inputIsValid() }
						onClick={ this._registrateUser } color='primary'>
						Registartion
					</Button>
					<Button disabled={ !this._inputIsValid() }
						onClick={ this._loginUser } color='primary'>
						Login
					</Button>
				</DialogActions>
			</main>
		);
	}
}