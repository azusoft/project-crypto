export default interface State {
	email: string;
	password: string;
	badEmail: boolean;
	badPassword: boolean;
}