export default interface Props {
	readonly title: string,
	readonly body: string,
	readonly props: any
}