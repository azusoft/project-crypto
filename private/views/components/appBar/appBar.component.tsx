import * as React from 'react';
import AppBar from 'material-ui/AppBar';
import BrowserUtility from '../../../libs/browserUtility';
import Props from './appBar.props';
import State from './appBar.state';
import Style from './appBar.style';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';

export default class extends React.Component<Props, State> {
	private _observer;

	constructor(props) {
		super(props);
		if (BrowserUtility.isBrowser) {
			this._observer = this.props.observer;
		}
		this.state = {
			containerViewHeight: 0
		}
	}

	render() {
		return (
			<AppBar position='static' color='primary'>
				<Toolbar>
					<Typography noWrap type='title' color='inherit' style={ Style.flex }>
						{ this.props.title }
					</Typography>
					{ this.props.children }
				</Toolbar>
			</AppBar>
		);
	}
}