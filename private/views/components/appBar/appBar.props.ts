import { ReactObserver } from 'react-event-observer';

export default interface Props {
	readonly title: string;
	readonly observer: ReactObserver;
}