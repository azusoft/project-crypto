import { Dark } from '../../themes';
import { findDOMNode } from 'react-dom';
import * as React from 'react';
import BrowserUtility from '../../../libs/browserUtility';
import Button from 'material-ui/Button';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Props from './chatViewSkeleton.props';
import State from './chatViewSkeleton.state';
import Style from './chatViewSkeleton.style';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import MessagingView from '../messagingView';

export default class extends React.Component<Props, State> {
	private _observer;

	constructor(props) {
		super(props);
		if (BrowserUtility.isBrowser) {
			this._changeContainerViewHeight = this._changeContainerViewHeight.bind(this);
			this._observer = this.props.observer;
		}
		this.state = {
			containerViewHeight: 0
		}
	}

	componentWillUnmount() {
		this._observer.unsubscribe('changeHeight');
	}

	componentDidMount() {
		this._observer.subscribe('changeHeight', this._changeContainerViewHeight);
		this._observer.publish('queryHeight');
	}

	_changeContainerViewHeight(containerViewHeight) {
		this.setState({ containerViewHeight });
	}

	render() {
		return (
			<main style={ Style.containerView(this.state.containerViewHeight) }>
				<Grid container spacing={ 8 } style={ Style.gridContainer }>
					<Grid item xs={ 12 }>
						<MessagingView observer={ this._observer }/>
					</Grid>
				</Grid>
			</main>
		)
	}
}