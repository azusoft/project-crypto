export default {
	containerView(height) {
		return {
			width: '100%',
			height: height,
			display: 'flex'
		}
	},
	gridContainer: {
		flex: 1,
		margin: 0
	}
}