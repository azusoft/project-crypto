import { findDOMNode } from 'react-dom';
import { IMessage } from '../../../models';
import * as React from 'react';
import * as Validator from 'validator';
import AppBar from 'material-ui/AppBar';
import BrowserUtility from '../../../libs/browserUtility';
import Button from 'material-ui/Button';
import List, { ListItem, ListItemText } from 'material-ui/List';
import Paper from 'material-ui/Paper';
import Props from './messagingView.props';
import State from './messagingView.state';
import Style from './messagingView.style';
import TextField from 'material-ui/TextField';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';

export default class extends React.Component<Props, State> {
	private _observer;

	constructor(props) {
		super(props);
		if (BrowserUtility.isBrowser) {
			this._observer = this.props.observer;
			this._changeContainerViewHeight = this._changeContainerViewHeight.bind(this);
			this._calculateNewMessageViewHeight = this._calculateNewMessageViewHeight.bind(this);
			this._sendMessage = this._sendMessage.bind(this);
			this._newMessage = this._newMessage.bind(this);
		}
		this.state = {
			containerViewHeight: 0,
			newMessageViewHeight: 0,
			message: '',
			messages: []
		}
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this._calculateNewMessageViewHeight);
		this._observer.unsubscribe('changeHeight');
	}

	componentDidMount() {
		window.addEventListener('resize', this._calculateNewMessageViewHeight);
		this._observer.subscribe('changeHeight', this._changeContainerViewHeight);
		this._observer.subscribe('newMessage', this._newMessage);
		this._observer.publish('queryHeight');
		this._calculateNewMessageViewHeight();
	}

	_calculateNewMessageViewHeight() {
		let height: number = findDOMNode(this.refs.newMessageView).clientHeight;
		this.setState({ newMessageViewHeight: height });
	}

	_changeContainerViewHeight(containerViewHeight) {
		this.setState({ containerViewHeight });
	}

	_newMessage(message: IMessage) {
		let messages = this.state.messages;
		messages.push(message);
		this.setState({ messages });
		let lastMessage = findDOMNode(this.refs.lastMessage);
		lastMessage.scrollIntoView({ behaviour: 'smooth' });
	}

	_handleChange = name => event => {
		if (event.target.value.length <= 150) {
			this.setState({ [name]: event.target.value });
		} else {
			//Do nothing
		}
	}

	_sendMessage() {
		if (this.state.message.trim().length > 1) {
			this._observer.publish('sendMessage', Validator.escape(this.state.message.trim()));
			this.setState({ message: '' });
		} else {
			//Do nothing
		}
	}

	render() {
		return (
			<Paper style={ Style.containerView(this.state.containerViewHeight, 8) }>
				<main style={ Style.listView(this.state.newMessageViewHeight) }>
					<List>
						{ this.state.messages.map((message, index) => <ListItem key={ index }>
							<ListItemText primary={ message.sender } secondary={ message.message } />
						</ListItem>) }
						<main ref='lastMessage' />
					</List>
				</main>
				<main ref='newMessageView' style={ Style.newMessageView }>
					<TextField
						autoFocus
						id='message'
						label={`Message ${ 150 - this.state.message.length } char left`}
						type='text'
						style={ Style.textField }
						value={ this.state.message }
						onChange={ this._handleChange('message') }
					/>
					<Button color='primary' style={ Style.sendButton } onClick={ this._sendMessage }>Send</Button>
				</main>
			</Paper>
		);
	}
}