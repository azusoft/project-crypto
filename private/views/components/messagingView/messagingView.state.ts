import { IMessage } from '../../../models';

export default interface State {
	containerViewHeight: number;
	newMessageViewHeight: number;
	message: string;
	messages: IMessage[];
}