export default {
	containerView(height, spacing) {
		return {
			height: height - spacing,
			overflow: 'hidden' as
				('auto' | 'hidden' | 'initial' | 'inherit' | 'unset' | 'scroll' | 'visible' | undefined)
		}
	},
	sendButton: {
		width: '50px',
		margin: 5
	},
	textField: {
		flex: 1,
		margin: 5
	},
	listView(height) {
		return {
			width: '100%',
			height: `calc(100% - ${ height }px)`,
			overflow: 'auto' as
				('auto' | 'hidden' | 'initial' | 'inherit' | 'unset' | 'scroll' | 'visible' | undefined),
		}
	},
	newMessageView: {
		display: 'flex'
	}
}