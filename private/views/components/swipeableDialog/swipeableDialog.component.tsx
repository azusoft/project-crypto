import { IUser } from '../../../models';
import * as React from 'react';
import BrowserUtility from '../../../libs/browserUtility';
import Dialog from 'material-ui/Dialog';
import Props from './swipeableDialog.props';
import State from './swipeableDialog.state';
import Style from './swipeableDialog.style';
import SwipeableViews from 'react-swipeable-views';


export default class extends React.Component<Props, State> {
	private _observer;

	constructor(props) {
		super(props);
		if (BrowserUtility.isBrowser) {
			this._swipeToScreen = this._swipeToScreen.bind(this);
			this._openDialog = this._openDialog.bind(this);
			this._closeDialog = this._closeDialog.bind(this);
			this._closeDialogPublish = this._closeDialogPublish.bind(this);
			this._observer = this.props.observer;
		}
		this.state = {
			isVisible: false,
			screen: 0,
		}
	}

	componentWillUnmount() {
		this._observer.unsubscribe('swipeToScreen');
		this._observer.unsubscribe('openDialog');
		this._observer.unsubscribe('closeDialog');
	}

	componentDidMount() {
		this._observer.subscribe('swipeToScreen', this._swipeToScreen);
		this._observer.subscribe('openDialog', this._openDialog);
		this._observer.subscribe('closeDialog', this._closeDialog);
	}
	
	_swipeToScreen({ id, screen }) {
		if (React.Children.count(this.props.children) >= screen && id === this.props.id) {
			this.setState({ screen: screen });
		} else {
			//Do nothing
		}
	}

	_openDialog(id: string) {
		if (id === this.props.id) {
			this.setState({ isVisible: true });
		} else {
			//Do nothing
		}
	}

	_closeDialog(id: string) {
		if (id === this.props.id) {
			this.setState({ isVisible: false });
		} else {
			//Do nothing
		}
	}

	_closeDialogPublish() {
		this._observer.publish('closeDialog', this.props.id);
	}

	render() {
		return (
			<Dialog onBackdropClick={ this._closeDialogPublish } style={ Style.dialogContainer }
				onEscapeKeyDown={ this._closeDialogPublish } open={this.state.isVisible}>
				<SwipeableViews disabled axis='x' index={this.state.screen} style={ Style.flex }>
					{ React.Children.map(this.props.children, child => {
						return React.cloneElement(child as React.ReactElement<any>, { id: this.props.id });
					}) }
				</SwipeableViews>
			</Dialog>
		);
	}
}