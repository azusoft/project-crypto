export default interface State {
	isVisible: boolean;
	screen: number;
}