import { IUser } from '../../../models';
import * as React from 'react';
import BrowserUtility from '../../../libs/browserUtility';
import Button from 'material-ui/Button';
import { DialogActions, DialogContent, DialogContentText, DialogTitle } from 'material-ui/Dialog';
import Props from './loadingDialog.props';
import State from './loadingDialog.state';
import Style from './loadingDialog.style';
import TextField from 'material-ui/TextField';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import { LinearProgress } from 'material-ui/Progress';

export default class extends React.Component<Props, State> {
	private _observer;
	private _dialogTitleSize: number = 0;
	private _dialogActionsSize: number = 0;

	constructor(props) {
		super(props);
		if (BrowserUtility.isBrowser) {
			this._closeLoginDialog = this._closeLoginDialog.bind(this);
			this._observer = this.props.observer;
		}
	}

	_closeLoginDialog() {
		this._observer.publish('closeDialog', this.props.id);
	}

	render() {
		return (
			<main style={ Style.mainView }>
				<LinearProgress style={ Style.flex }/>
			</main>
		);
	}
}