enum EFlexDirection {
	'flex-start', 'flex-end', 'center', 'space-between', 'space-around', 'space-evenly'
}

enum EJustifyContent {
	'flex-start', 'flex-end', 'center', 'space-between', 'space-around', 'space-evenly'
}

export default {
	mainView: {
		display: 'flex',
		flexDirection: 'column' as
			('column' | 'initial' | 'inherit' | 'unset' | 'row' | 'row-reverse' | 'column-reverse' | undefined),
		height: '100%'
	},
	flex: {
		flex: 1
	},
	centerView: {
		display: 'flex'
	}
}