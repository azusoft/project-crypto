import { ReactObserver } from 'react-event-observer';

export default interface Props {
	readonly observer: ReactObserver;
	readonly id?: string;
}