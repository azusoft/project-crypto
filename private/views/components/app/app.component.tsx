import { Dark } from '../../themes';
import { findDOMNode } from 'react-dom';
import * as React from 'react';
import AppBar from '../appBar';
import BrowserUtility from '../../../libs/browserUtility';
import Button from 'material-ui/Button';
import ChatViewSkeleton from '../chatViewSkeleton';
import LoadingDialog from '../loadingDialog';
import LoginDialog from '../loginDialog';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Props from './app.props';
import State from './app.state';
import Style from './app.style';
import SwipeableDialog from '../swipeableDialog';

export default class extends React.Component<Props, State> {
	private _observer;

	constructor(props) {
		super(props);
		if (BrowserUtility.isBrowser) {
			this._calculateContainerHeight = this._calculateContainerHeight.bind(this);
			this._showLoginDialog = this._showLoginDialog.bind(this);
			this._changeContainerViewHeight = this._changeContainerViewHeight.bind(this);
			this._acceptLoginUser = this._acceptLoginUser.bind(this);
			this._logout = this._logout.bind(this);
			this._observer = this.props.observer;
		}
		this.state = {
			containerViewHeight: 0,
			userIsLoggedIn: false
		}
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this._calculateContainerHeight);
		this._observer.unsubscribe('queryHeight');
	}

	componentDidMount() {
		window.addEventListener('resize', this._calculateContainerHeight);
		this._observer.subscribe('queryHeight', this._calculateContainerHeight);
		this._observer.subscribe('changeHeight', this._changeContainerViewHeight);
		this._observer.subscribe('acceptUserLogin', this._acceptLoginUser);
		this._observer.publish('queryHeight');
	}

	_calculateContainerHeight() {
		let height: number = findDOMNode(this.refs.AppBar).clientHeight;
		this._observer.publish('changeHeight', window.innerHeight - height);
	}
	
	_showLoginDialog() {
		this._observer.publish('openDialog', 'LoginDialog');
	}

	_changeContainerViewHeight(containerViewHeight) {
		this.setState({ containerViewHeight });
	}

	_acceptLoginUser() {
		this.setState({ userIsLoggedIn: true });
	}

	_logout() {
		this._observer.publish('logoutUser');
		this.setState({ userIsLoggedIn: false });
	}

	render() {
		return (
			<MuiThemeProvider theme={ Dark }>
				<AppBar ref='AppBar' title='Project Crypto' observer={ this._observer }>
					{ this.state.userIsLoggedIn
					? <main>
						<Button color='inherit' onClick={ this._logout }>Logout</Button>
					</main> : <main>
						<Button color='inherit' onClick={ this._showLoginDialog }>Login</Button>
					</main>
					}
				</AppBar>
				<main style={ Style.containerView(this.state.containerViewHeight) }>
					{ this.state.userIsLoggedIn
					? <ChatViewSkeleton observer={ this._observer }/>
					: <SwipeableDialog observer={ this._observer } id='LoginDialog'>
						<LoginDialog observer={ this._observer }/>
						<LoadingDialog observer={ this._observer }/>
					</SwipeableDialog>
					}
				</main>
			</MuiThemeProvider>
		)
	}
}