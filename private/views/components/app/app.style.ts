export default {
	containerView(height) {
		return {
			width: '100%',
			height: height,
			display: 'flex'
		}
	}
};