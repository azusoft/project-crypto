export default interface State {
	containerViewHeight: number;
	userIsLoggedIn: boolean;
}