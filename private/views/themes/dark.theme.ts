import { red, indigo, lightBlue } from 'material-ui/colors';
import createMuiTheme from 'material-ui/styles/createMuiTheme';
import createPalette from 'material-ui/styles/createPalette';

export default createMuiTheme({
	palette: createPalette({
		primary: indigo,
		secondary: lightBlue,
		error: red,
		type: 'light'
	})
});