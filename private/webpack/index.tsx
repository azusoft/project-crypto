import { hydrate } from 'react-dom';
import { Logger } from '../libs/logger';
import * as React from 'react';
import App from '../views/app';

try {
	let unparsedProps = document.getElementById('props');
	if (unparsedProps && unparsedProps.innerHTML) {
		let props = JSON.parse(unparsedProps.innerHTML);
		hydrate(<App {...props} />, document.getElementById('view'));
	} else {
		throw new Error('No props');
	}
} catch(err) {
	console.error(err.message);
}