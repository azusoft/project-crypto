import * as Env from 'env-var';
import * as Database from 'better-sqlite3';
import { Logger } from '../logger';
import { IUser } from '../../models';
import * as UUID from 'uuid-js';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/defer';

/**
 * The SqLite.
 * By: David Zarandi (Azuwey)
 *
 * @class SqLite
 */
export class SqLite {
	private static _instance: SqLite = new SqLite();
	private _db: Database;

	/**
	 * Constructor.
	 * 
	 * @class SqLite
	 * @constructor
	 */
	constructor() {
		if (SqLite._instance){
			throw new Error('Error: Instantiation failed: Use SqLite.getInstance() instead of new.');
		} else {
			let file = '../../../projectcrypto.db';
			SqLite._instance = this;
			this._db = new Database(Env.get('DATABASEURI').asString() || 
				require('path').resolve(__dirname, file));
			let sql_user_table = 'CREATE TABLE IF NOT EXISTS users (' +
				'id VARCHAR PRIMARY KEY,' +
				'email VARCHAR NOT NULL,' +
				'password VARCHAR NOT NULL' +
				')';
			this._db.exec(sql_user_table);
		}
	}

	/**
	 * Return singleton instance
	 *
	 * @class SqLite
	 * @method getInstance
	 * @returns SqLite
	 */
	public static getInstance(): SqLite {
		return SqLite._instance;
	}

	public newUser(user: any): IUser | boolean {
		let _user = user.user;
		let row = this._db.prepare('SELECT id id FROM users WHERE email = ?')
			.get(_user.email);
		if (row === undefined) {
			_user.id = UUID.create(4).hex;
			let stmt = this._db.prepare('INSERT INTO users (id, email, password) VALUES' +
				'(@id, @email, @password)');
			stmt.run({
				id: _user.id,
				email: _user.email,
				password: _user.password
			});
			return _user;
		} else {
			return false;
		}
	}

	public loginUser(user: any): IUser {
		let _user = user.user;
		let row = this._db.prepare('SELECT id id FROM users WHERE email = ? AND password = ?')
			.get(_user.email, _user.password);
		return row;
	}
}