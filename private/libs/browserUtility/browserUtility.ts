/**
 * The browser utility.
 * By: David Zarandi (Azuwey)
 * 2017
 *
 * @class BrowserUtility
 */
export default class BrowserUtility {
	public static get isBrowser() {
		return (typeof window !== 'undefined' && window.document && window.document.createElement);
	}
}