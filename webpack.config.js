const path = require('path');

module.exports = {
	devtool: 'inline-source-map',
	entry: './private/webpack/index.tsx',
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				use: 'ts-loader',
				exclude: '/node_modules/'
			}
		]
	},
	resolve: {
		extensions: [ '.tsx', '.ts', '.js' ]
	},
	/*include: {
		'./private/webpack/',
		'/private/webpack/'
	}*/
	output: {
		filename: 'bundle.js',
		path: path.resolve(__dirname, 'public', 'static')
	}
};
